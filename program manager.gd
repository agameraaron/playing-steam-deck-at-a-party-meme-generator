extends Control

var program_version = [0,1,0,0]

func _ready():
	_on_load_video_pressed()

onready var file_dialog = get_node("file dialog")
func _on_load_video_pressed():
	file_dialog.popup()
	#For some reason when it pops up it goes to the right and down a bit,
	#  this sets it's rect back to 0,0 where it belongs.
	file_dialog._set_position(Vector2(0,0))

onready var video_player = get_node("preview panel/video viewport/video player")
onready var background = get_node("preview panel/background")
var video_file = null
func _on_file_dialog_file_selected(path):
	video_file = File.new()
	video_file.open(path,File.READ)
	video_player.set_stream(load(path))
	video_file.close()
	background.show()
	video_player.play()
	

#Currently closes program entirely.
func _on_file_dialog_popup_hide():
	if video_file == null:
		get_tree().quit()

