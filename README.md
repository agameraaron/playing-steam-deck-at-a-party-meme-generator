# Playing Steam Deck at a Party Meme Generator

A simple application I made to easily create your own version of the "Playing Steam Deck at a Party" funny video meme.

It does not generate a video file. That would be neat, but I don't see any easy way of doing that in Godot.
What you have to do is record the window with OBS for now. Sorry.

Because I can't scale the screen polygon to the 'party background' image properly the window is locked at 720p.
I can probably do something about this, but I failed a few times in doing so already so I just locked the resolution for now.
The node for the polygon is a 2D Node with no Control node alternative whereas the image is a Control node so that it properly scales with the interface. I'd have to find some kind of workaround for one of these.

I only tried it with ".webm" files, but it should also work with ".ogg" and ".ogm" files as well.
Sorry to say that's all Godot supports as it is, but modules may be an option for an update allowing more formats.
